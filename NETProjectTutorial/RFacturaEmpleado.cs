﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace NETProjectTutorial
{
    public partial class RFacturaEmpleado : Form
    {
        private DataTable dtFEmpleado;
        private DataSet dsFEmpleado;
        private BindingSource bsFEmpleado;
        private DataRow drFEmpleado;
        
        string emplead,cod, subtotal, iva, total;
        DateTime fecha;

        private void RFacturaEmpleado_Load(object sender, EventArgs e)
        {
            bsFEmpleado.DataSource = DsFEmpleado;
            bsFEmpleado.DataMember = DsFEmpleado.Tables["Factura"].TableName;
            //F
        }

        public DataRow DrFEmpleado
        {
            set
            {
                drFEmpleado = value;
                emplead = drFEmpleado["Nombre_Empleado"].ToString();
                cod = drFEmpleado["Cod_Factura"].ToString();
                fecha = Convert.ToDateTime(drFEmpleado["Fecha"].ToString());
                subtotal = drFEmpleado["Subtotal"].ToString();
                iva = drFEmpleado["IVA"].ToString();

            }
        }

        public RFacturaEmpleado()
        {
            InitializeComponent();
            bsFEmpleado = new BindingSource();
        }

        public DataTable DtFEmpleado { get => dtFEmpleado; set => dtFEmpleado = value; }
        public DataSet DsFEmpleado { get => dsFEmpleado; set => dsFEmpleado = value; }
    }
}

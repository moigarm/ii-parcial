﻿using NETProjectTutorial.dao;
using NETProjectTutorial.entities;
using NETProjectTutorial.util;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static NETProjectTutorial.entities.Empleado;

namespace NETProjectTutorial.implements
{
    class DaoImplementsEmpleado : IDaoEmpleado
    {
        //header cliente
        private BinaryReader brhempleado;
        private BinaryWriter bwhempleado;
        //data cliente
        private BinaryReader brdempleado;
        private BinaryWriter bwdempleado;

        private FileStream fshempleado;
        private FileStream fsdempleado;
        private const string FILENAME_HEADER = "hempleado.dat";
        private const string FILENAME_DATA = "dempleado.dat";
        private const int SIZE = 435;

        public DaoImplementsEmpleado()
        {
            RandomFileBinarySearch.SIZE = 4;
        }

        private void open()
        {
            try
            {
                fsdempleado = new FileStream(FILENAME_DATA, FileMode.OpenOrCreate, FileAccess.ReadWrite);

                if (!File.Exists(FILENAME_HEADER))
                {
                    fshempleado = new FileStream(FILENAME_HEADER, FileMode.OpenOrCreate, FileAccess.ReadWrite);
                    brhempleado = new BinaryReader(fshempleado);
                    bwhempleado = new BinaryWriter(fshempleado);
                    brdempleado = new BinaryReader(fsdempleado);
                    bwdempleado = new BinaryWriter(fsdempleado);
                    bwhempleado.BaseStream.Seek(0, SeekOrigin.Begin);
                    bwhempleado.Write(0); //n
                    bwhempleado.Write(0); //k
                }
                else
                {
                    fshempleado = new FileStream(FILENAME_HEADER, FileMode.OpenOrCreate, FileAccess.ReadWrite);
                    brhempleado = new BinaryReader(fshempleado);
                    bwhempleado = new BinaryWriter(fshempleado);
                    brdempleado = new BinaryReader(fsdempleado);
                    bwdempleado = new BinaryWriter(fsdempleado);
                }
            }
            catch (IOException e)
            {
                throw new IOException(e.Message);
            }
        }

        private void close()
        {
            {
                try
                {
                    if (brdempleado != null)
                    {
                        brdempleado.Close();
                    }
                    if (brhempleado != null)
                    {
                        brhempleado.Close();
                    }
                    if (bwdempleado != null)
                    {
                        bwdempleado.Close();
                    }
                    if (brdempleado != null)
                    {
                        brdempleado.Close();
                    }
                    if (fsdempleado != null)
                    {
                        fsdempleado.Close();
                    }
                    if (fshempleado != null)
                    {
                        fshempleado.Close();
                    }
                }
                catch (IOException e)
                {

                    throw new IOException(e.Message);
                }
            }
        }

        public Empleado findById(string Id)
        {
            throw new NotImplementedException();
        }

        public List<Empleado> findByLastname(string lastname)
        {
            throw new NotImplementedException();
        }

        public Empleado findByCedula(string cedula)
        {
            throw new NotImplementedException();
        }

        public void save(Empleado t)
        {
            open();
            brhempleado.BaseStream.Seek(0, SeekOrigin.Begin);
            int n = brhempleado.ReadInt32();
            int k = brhempleado.ReadInt32();

            long dpos = k * SIZE;
            bwdempleado.BaseStream.Seek(dpos, SeekOrigin.Begin);

            bwdempleado.Write(++k);
            bwdempleado.Write(t.Nombres);
            bwdempleado.Write(t.Apellidos);
            bwdempleado.Write(t.Cedula);
            bwdempleado.Write(t.Inss);
            bwdempleado.Write(t.Direccion);
            bwdempleado.Write(t.Salario);
            bwdempleado.Write(t.Tconvencional);
            bwdempleado.Write(t.Tcelular);
            bwdempleado.Write(t.Sexo.ToString());

            bwhempleado.BaseStream.Seek(0, SeekOrigin.Begin);
            bwhempleado.Write(++n);
            bwhempleado.Write(k);

            long hpos = 8 + (n - 1) * 4;
            bwhempleado.BaseStream.Seek(hpos, SeekOrigin.Begin);
            bwhempleado.Write(k);
            close();
        }

        public int update(Empleado t)
        {
            open();
            brhempleado.BaseStream.Seek(0, SeekOrigin.Begin);
            RandomFileBinarySearch.brhcliente = brhempleado;
            int n = brhempleado.ReadInt32();
            int pos = RandomFileBinarySearch.runBinarySearchRecursively(t.Id, 0, n);
            if (pos < 0)
            {
                close();
                return pos;
            }

            long hpos = 8 + 4 * (pos);
            brhempleado.BaseStream.Seek(hpos, SeekOrigin.Begin);
            int id = brhempleado.ReadInt32();

            long dpos = (id - 1) * SIZE;
            brdempleado.BaseStream.Seek(dpos, SeekOrigin.Begin);

            bwdempleado.Write(t.Id);
            bwdempleado.Write(t.Nombres);
            bwdempleado.Write(t.Apellidos);
            bwdempleado.Write(t.Cedula);
            bwdempleado.Write(t.Inss);
            bwdempleado.Write(t.Direccion);
            bwdempleado.Write(t.Salario);
            bwdempleado.Write(t.Tconvencional);
            bwdempleado.Write(t.Tcelular);
            bwdempleado.Write(t.Sexo.ToString());

            close();
            return t.Id;
        }

        public bool delete(Empleado t)
        {
            throw new NotImplementedException();
        }

        public List<Empleado> findAll()
        {
            open();
            List<Empleado> empleados = new List<Empleado>();

            brhempleado.BaseStream.Seek(0, SeekOrigin.Begin);
            int n = brhempleado.ReadInt32();

            for (int i = 0; i < n; i++)
            {
                //Calculamos posición cabecera
                long hpos = 8 + i * 4;
                brhempleado.BaseStream.Seek(hpos, SeekOrigin.Begin);
                int index = brhempleado.ReadInt32();

                //Calculamos posición de los datos
                long dpos = (index - 1) * SIZE;
                brdempleado.BaseStream.Seek(dpos, SeekOrigin.Begin);

                int id = brdempleado.ReadInt32();
                string nombre = brdempleado.ReadString();
                string apellidos = brdempleado.ReadString();
                string cedula = brdempleado.ReadString();
                string inss = brdempleado.ReadString();
                string direccion = brdempleado.ReadString();
                double salario = brdempleado.ReadDouble();
                string tconvencional = brdempleado.ReadString();
                string tcelular = brdempleado.ReadString();
                SEXO sexo = (SEXO)brdempleado.Read();
                //Empleado  = Enum.Parse(typeof(Empleado), sexo, true);

                Empleado e = new Empleado(id, nombre, apellidos, cedula, inss, direccion, salario, tconvencional, tcelular, sexo);
                empleados.Add(e);
            }

            close();
            return empleados;
        }
    }
}

﻿using System;
using NETProjectTutorial.model;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace NETProjectTutorial
{
    public partial class FrmGestionClientes : Form
    {
        private DataSet dsClientes;
        private BindingSource bsClientes;
        private ClienteModel clienteModel;
        DataTable dtClientesDeletedCopy;

        public DataSet DsClientes { get => dsClientes; set => dsClientes = value; }

        public FrmGestionClientes()
        {
            InitializeComponent();
            bsClientes = new BindingSource();
            clienteModel = new ClienteModel();
            
        }
        private void FrmGestionClientes_Load(object sender, EventArgs e)
        {
            bsClientes.DataSource = DsClientes;
            bsClientes.DataMember = DsClientes.Tables["Cliente"].TableName;
            dgvClientes.DataSource = bsClientes;
            dgvClientes.AutoGenerateColumns = true;

            dtClientesDeletedCopy = dsClientes.Tables["Cliente"].Clone();
        }

        private void BtnNuevo_Click(object sender, EventArgs e)
        {
            FrmCliente fc = new FrmCliente();
            fc.TblClientes = dsClientes.Tables["Cliente"];
            fc.DsClientes = DsClientes;
            fc.ShowDialog();
        }

        private void BtnEditar_Click(object sender, EventArgs e)
        {
            DataGridViewSelectedRowCollection rowCollection = dgvClientes.SelectedRows;

            if (rowCollection.Count == 0)
            {
                MessageBox.Show(this, "Debe seleccionar una fila de la tabla para poder editar", "Mensaje de Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            DataGridViewRow gridRow = rowCollection[0];
            DataRow drow = ((DataRowView)gridRow.DataBoundItem).Row;

            FrmCliente fc = new FrmCliente();
            fc.TblClientes = DsClientes.Tables["Cliente"];
            fc.DsClientes = DsClientes;
            fc.DrCliente = drow;
            fc.ShowDialog();
        }

        private void BtnEliminar_Click(object sender, EventArgs e)
        {
            DataGridViewSelectedRowCollection rowCollection = dgvClientes.SelectedRows;

            if (rowCollection.Count == 0)
            {
                MessageBox.Show(this, "Debe seleccionar una fila de la tabla para poder editar", "Mensaje de Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            DataGridViewRow gridRow = rowCollection[0];
            DataRow drow = ((DataRowView)gridRow.DataBoundItem).Row;

            DialogResult result = MessageBox.Show(this, "¿Realmente desea eliminar este registro?", "Mensaje del Sistema", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            DataRow copyDrow = dtClientesDeletedCopy.NewRow();
            if (result == DialogResult.Yes)
            {
                copyDrow.ItemArray = drow.ItemArray;
                DsClientes.Tables["Cliente"].Rows.Remove(drow);
                
                dtClientesDeletedCopy.Rows.Add(copyDrow);
                

                MessageBox.Show(this, "Registro eliminado satisfactoriamente", "Mensaje del Sistema", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void TxtFinder_TextChanged(object sender, EventArgs e)
        {
            try
            {
                bsClientes.Filter = string.Format("Cédula like '*{0}*' or Nombres like '*{0}*' or Apellidos like '*{0}*' ", txtFinder.Text);

            }
            catch (InvalidExpressionException ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        private void FrmGestionClientes_FormClosing(object sender, FormClosingEventArgs e)
        {
            //DataTable dtClientesAdded = dsClientes.Tables["Cliente"].GetChanges(DataRowState.Added);
            //DataTable dtClientesUpdated = dsClientes.Tables["Cliente"].GetChanges(DataRowState.Modified);
            //DataTable dtClientesDeleted = dsClientes.Tables["Cliente"].GetChanges(DataRowState.Deleted);

            //if (dtClientesAdded != null)
            //{
            //    foreach (DataRow dr in dtClientesAdded.Rows)
            //    {
            //        clienteModel.save(dr);
            //        dsClientes.Tables["Cliente"].Rows.Find(dr["Id"]).AcceptChanges();
            //    }
            //}

            //if (dtClientesUpdated != null)
            //{
            //    foreach (DataRow dr in dtClientesUpdated.Rows)
            //    {
            //        clienteModel.update(dr);
            //        dsClientes.Tables["Cliente"].Rows.Find(dr["Id"]).AcceptChanges();
            //    }

            //}

            //if (dtClientesDeletedCopy.Rows.Count > 0)
            //{
            //    foreach (DataRow dr in dtClientesDeletedCopy.Rows)
            //    {
            //        clienteModel.delete(dr);
                    
            //    }
            //}
        }
    }
}

﻿using NETProjectTutorial.entities;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NETProjectTutorial.model
{
    class EmpleadoModel
    {
        private static List<Empleado> ListEmpleados = new List<Empleado>();
        private implements.DaoImplementsEmpleado daoempleado;

        public EmpleadoModel()
        {
            daoempleado = new implements.DaoImplementsEmpleado();
        }

        public List<Empleado> GetListEmpleado()
        {
            return daoempleado.findAll();
        }

        public void Populate()
        {
            ListEmpleados = JsonConvert.DeserializeObject<List<Empleado>>(System.Text.Encoding.Default.GetString(NETProjectTutorial.Properties.Resources.Empleado_data));

            foreach (Empleado e in ListEmpleados)
            {
                daoempleado.save(e);
            }
        }

        public void save(DataRow empleado)
        {
            Empleado e = new Empleado();
            e.Id = Convert.ToInt32(empleado["Id"].ToString());
            e.Inss = empleado["INSS"].ToString();
            e.Cedula = empleado["Cédula"].ToString();
            e.Nombres = empleado["Nombres"].ToString();
            e.Apellidos = empleado["Apellidos"].ToString();
            e.Direccion = empleado["Dirección"].ToString();
            e.Tconvencional = empleado["Teléfono"].ToString();
            e.Tcelular = empleado["Celular"].ToString();
            //e.Sexo = empleado["Sexo"].ToString();

            daoempleado.save(e);
        }

        public void update(DataRow empleado)
        {
            Empleado e = new Empleado();
            e.Id = Convert.ToInt32(empleado["Id"].ToString());
            e.Inss = empleado["INSS"].ToString();
            e.Cedula = empleado["Cédula"].ToString();
            e.Nombres = empleado["Nombres"].ToString();
            e.Apellidos = empleado["Apellidos"].ToString();
            e.Direccion = empleado["Dirección"].ToString();
            e.Tconvencional = empleado["Teléfono"].ToString();
            e.Tcelular = empleado["Celular"].ToString();
            //e.Sexo = empleado["Sexo"].ToString();

            daoempleado.update(e);
        }

        public void delete(DataRow empleado)
        {
            Empleado e = new Empleado();
            e.Id = Convert.ToInt32(empleado["Id"].ToString());
            e.Inss = empleado["INSS"].ToString();
            e.Cedula = empleado["Cédula"].ToString();
            e.Nombres = empleado["Nombres"].ToString();
            e.Apellidos = empleado["Apellidos"].ToString();
            e.Direccion = empleado["Dirección"].ToString();
            e.Tconvencional = empleado["Teléfono"].ToString();
            e.Tcelular = empleado["Celular"].ToString();
            //e.Sexo = empleado["Sexo"].ToString();

            daoempleado.delete(e);
        }
    }
}

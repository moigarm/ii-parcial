﻿using NETProjectTutorial.entities;
using NETProjectTutorial.model;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace NETProjectTutorial
{
    public partial class FrmMain : Form
    {
        private DataTable dtProductos;

        public FrmMain()
        {
            InitializeComponent();
        }

        private void productosToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FrmGestionProductos fgp = new FrmGestionProductos();
            fgp.MdiParent = this;
            fgp.DsProductos = dsProductos;
            fgp.Show();
        }

        private void FrmMain_Load(object sender, EventArgs e)
        {
            dtProductos = dsProductos.Tables["Producto"];

            ProductoModel.Populate();
            foreach (Producto p in ProductoModel.GetProductos())
            {
                dtProductos.Rows.Add(p.Id, p.Sku, p.Nombre, p.Descripcion, p.Cantidad, p.Precio, p.Sku + " - " + p.Nombre);
            }

            new EmpleadoModel().Populate();
            /*foreach (Empleado employee in new EmpleadoModel().GetListEmpleado())
            {
                DataRow drEmpleado = dsProductos.Tables["Empleado"].NewRow();
                drEmpleado["Id"] = employee.Id;
                drEmpleado["INSS"] = employee.Inss;
                drEmpleado["Cédula"] = employee.Cedula;
                drEmpleado["Nombres"] = employee.Nombres;
                drEmpleado["Apellido"] = employee.Apellidos;
                drEmpleado["Dirección"] = employee.Direccion;
                drEmpleado["Teléfono"] = employee.Tconvencional;
                drEmpleado["Celular"] = employee.Tcelular;
                drEmpleado["Salario"] = employee.Salario;
                drEmpleado["Sexo"] = employee.Sexo;
                dsProductos.Tables["Empleado"].Rows.Add(drEmpleado);
                drEmpleado.AcceptChanges();

            }*/

            //new ClienteModel().Populate();
            foreach (Cliente client in new ClienteModel().GetListCliente())
            {
                DataRow drCliente = dsProductos.Tables["Cliente"].NewRow();
                drCliente["Id"] = client.Id;
                drCliente["Cédula"] = client.Cedula;
                drCliente["Nombres"] = client.Nombres;
                drCliente["Apellidos"] = client.Apellidos;
                drCliente["Teléfono"] = client.Telefono;
                drCliente["Correo"] = client.Correo;
                drCliente["Dirección"] = client.Direccion;
                dsProductos.Tables["Cliente"].Rows.Add(drCliente);
                drCliente.AcceptChanges();
            }
        }

        private void EmpleadosToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FrmGestionEmpleados fge = new FrmGestionEmpleados();
            fge.MdiParent = this;
            fge.DsEmpleados = dsProductos;
            fge.Show();
        }

        private void NuevaFacturaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FrmFactura ff = new FrmFactura();
            ff.MdiParent = this;
            ff.DsSistema = dsProductos;
            ff.Show();
        }

        private void FacturasToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FrmGestionFacturas fgf = new FrmGestionFacturas();
            fgf.MdiParent = this;
            fgf.DsFacturas = dsProductos;
            fgf.Show();
        }

        private void ClientesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FrmGestionClientes fgc = new FrmGestionClientes();
            fgc.MdiParent = this;
            fgc.DsClientes = dsProductos;
            fgc.Show();
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;


namespace NETProjectTutorial.util
{
    class RandomFileBinarySearch
    {
        public static BinaryReader brhcliente;
        public static int SIZE;

        public static int runBinarySearchRecursively(int key, int low, int high)
        {
            if (brhcliente == null)
            {
                return -1;
            }
            int middle = (low + high) / 2;

            if (high < low)
            {
                return -1;
            }

            long pos = 8 + SIZE * middle;
            brhcliente.BaseStream.Seek(pos, SeekOrigin.Begin);
            int id = brhcliente.ReadInt32();

            if (key == id)
            {
                return middle;
            }
            else if (key < id)
            {
                return runBinarySearchRecursively(
                        key, low, middle - 1);
            }
            else
            {
                return runBinarySearchRecursively(
                        key, middle + 1, high);
            }
        }
    }
}
